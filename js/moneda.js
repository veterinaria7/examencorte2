var cantidad = 0;
var monedaOrigen = 0;
var monedaDestino = 0;
var subTotal = 0;
var TotalComision = 0;
var totalPagar = 0;

function calcular(){
    cantidad = document.getElementById('cantidad').value;
    let selectValueMonedaDestino = document.getElementById('monedades').value;

    switch (selectValueMonedaDestino) {
        case 'PesoMex':
            monedaDestino = 19.85;
            break;
        case 'DolarUSA':
            monedaDestino = 1;
            break;
        case 'DolarCanadiense':
            monedaDestino = 1.35;
            break;
        case 'Euro':
            monedaDestino = 0.99;
            break;
            
    
    }

    subTotal = cantidad * monedaDestino;
    TotalComision = monedaDestino * 0.03;
    totalPagar = subTotal - TotalComision;

    document.getElementById("subtotal").value = subTotal;
    document.getElementById("totalcomi").value = TotalComision;
    document.getElementById("totalpag").value = totalPagar;

    // Moneda Origen Nombre
    let selectorOrigen = document.getElementById("monedaori");
    let nombreMonedaOrigen = selectorOrigen.options[selectorOrigen.selectedIndex].text;
    // Moneda Destino Nombre
    let selectorDestino = document.getElementById("monedades");
    let nombreMonedaDestino = selectorDestino.options[selectorDestino.selectedIndex].text;

    // Diseño de la tabla
    var myHtmlContent = `
        <tr>
            <td>${cantidad}</td>
        </tr>
        <tr>
            <td>${nombreMonedaOrigen}</td>
        </tr>
        <tr>
            <td>${nombreMonedaDestino}</td>
        </tr>
        <tr>
            <td>${subTotal}</td>
        </tr>
        <tr>
            <td>${TotalComision}</td>
        </tr>
        <tr>
            <td>${totalPagar}</td>
        </tr>
    `;
    // Agregar a la tabla
    document.getElementById("myTable").innerHTML += myHtmlContent
}

function ocultarSelector(){
    let selectOrigen = document.getElementById("monedaori");
    let selectDestino = document.getElementById("monedaori");
    let hide = selectDestino[selectOrigen.selectedIndex];
    hide.setAttribute('hidden', 'hidden');
}
function limpiar() {
    var cantidad = "";
    var monedaOrigen = "";
    var monedaDestino = "";
    var subTotal = "";
    var TotalComision = "";
    var totalPagar = "";

}
function registrar(){

}
function borrar(){
    
}